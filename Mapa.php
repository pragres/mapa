<?php

include "php-static-maps-generator/googlestaticmap.php";

class Mapa extends Service
{
	/**
	 * Load the Service when an email request arrives
	 * 
	 * @param Request
	 * */
	public function main($request){
		// create new static map
		$oStaticMap = new GoogleStaticMap();
		$oStaticMap->setHeight(400);
		$oStaticMap->setWidth(640);
		$oStaticMap->setHttps(true);
		$oStaticMap->setMapType("hybrid");

		// detecting zoom
		$zoom = null;
		for($i = 22; $i >= 1; $i --)
		{
			if (stripos($request->argument, $i . 'x') !== false)
			{
				$zoom = $i;
				$request->argument = str_ireplace($i . 'x', '', $request->argument);
			}
		}
		if ( ! is_null($zoom)) $oStaticMap->setZoom($zoom);

		// detecting type
		$type = 'hibrido';
		if (stripos($request->argument, 'fisico') !== false)
		{
			$oStaticMap->setMapType("satellite");
			$request->argument = str_replace_count('fisico', '', $request->argument, 1);
			$type = 'fisico';
		} 
		elseif (stripos($request->argument, 'politico') !== false)
		{
			$request->argument = str_replace_count('politico', '', $request->argument, 1);
			$oStaticMap->setMapType("roadmap");
			$type = 'politico';
		} 
		elseif (stripos($request->argument, 'terreno') !== false)
		{
			$request->argument = str_replace_count('terreno', '', $request->argument, 1);
			$oStaticMap->setMapType("terrain");
			$type = 'terreno';
		}

		if (stripos($request->argument, 'hibrido') !== false)
		{
			$request->argument = str_replace_count('hibrido', '', $request->argument, 1);
		}

		// scale map image
		$oStaticMap->setScale(1);
		$oStaticMap->setHeight(640);
		$oStaticMap->setWidth(640);

		$oStaticMap->setLanguage("es");
		$request->argument = trim($request->argument);
		if (substr($request->argument, 0, 3) == 'de ') $request->argument = substr($request->argument, 3);
		if (substr($request->argument, 0, 4) == 'del ') $request->argument = substr($request->argument, 4);
		$oStaticMap->setCenter("{$request->argument}");
		$img = file_get_contents($oStaticMap);

		// create a json object to send to the template
		$json = '
		{
			"title": "Mapa de '.$request->argument.' - '.date("Y-m-d").'",
			"argument": "'.$type.' '. $request->argument.'",
			"zoom": "'.$zoom.'",
			"sharethis": "MAPA '.$request->argument.'"
		}';

		// attach the image to the response object
		$attachment = new Attachment("image/jpeg", base64_encode($img), "mapa.jpg", "mapa");
		$this->response->addAttachment($attachment);

		// configure the response object to use a user defined template
		$this->response->createUserDefinedResponse("basic.tpl", $json);
	}
}